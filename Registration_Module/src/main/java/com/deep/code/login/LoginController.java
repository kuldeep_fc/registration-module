package com.deep.code.login;

import java.net.InetAddress;
import java.time.LocalDateTime;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.deep.code.BaseResponse;

/*
 * Login Controller
 * 
 * Author :- Kuldeep Argal 29/07/2020
 * 
 * */

@RestController
@RequestMapping("/api/auth")
public class LoginController 
{  
	
    @Autowired
    private LoginRepository loginRepository;
    
    @Autowired
    HttpServletRequest req;
    
    /*
     * Registration Met 
     * */
	
    @PostMapping("/save")
    public BaseResponse saveAndUpdate(@RequestBody LoginDTO loginDTO) //
    {
    	BaseResponse response=new BaseResponse(true);
    	LoginDTO login=loginRepository.loginByEmail(loginDTO.getEmail());
    	if(login==null)
    	{
    		LoginDTO loginNew=new LoginDTO();
    		loginNew.setName(loginDTO.getName());
    		loginNew.setEmail(loginDTO.getEmail());
    		loginNew.setPassword(loginDTO.getPassword());
    		loginRepository.save(loginNew);
    		response.setSuccess(true);
			response.addMessage(BaseResponse.SAVE);
		    return response;
    	}
    	else
    	{
    		
    		response.setSuccess(false);
    		response.addMessage(BaseResponse.EMAIL);
    	    return response;
    		}
    	
    			
    }
    
    /*
     * Login Method with session
     * */
	
    @PostMapping("/login")
	public BaseResponse loginAuthentication(@RequestBody  LoginDTO login)throws Exception 
	{
		BaseResponse response=new BaseResponse(true);
		LoginDTO loginDTO=loginRepository.loginByEmail(login.getEmail());
		
		if(loginDTO==null)
		{
			   response.setSuccess(false);
			   response.addMessage(BaseResponse.FAILED);
			   return response;
		}
		else  
		{
			if(login.getEmail().equals(loginDTO.getEmail()))
			{
				    LoginDTO dto=new LoginDTO();
				    dto.setPassword(login.getPassword());
					dto.setEmail(login.getEmail());
					dto.setId(loginDTO.getId());
					// For Ip Address
				    InetAddress localhost = InetAddress.getLocalHost();
					dto.setIpAddress(localhost.getHostAddress().trim());
					//Brower Details
					dto.setBrowserName(req.getHeader("user-agent"));
					//For System Name
					dto.setSystemName(localhost.getHostName().trim());
					//Current Date
					LocalDateTime current = LocalDateTime.now();
					dto.setLoginTimeDate(current);
					loginRepository.save(dto);
					
					 HttpSession session = req.getSession();
					req.getSession().setAttribute("SESSION",dto.getEmail());
					response.setSuccess(true);
					response.addMessage(BaseResponse.SUCCESS);
					
					return response;
			 }
			    response.setSuccess(false);
			    response.addMessage(BaseResponse.FAILED);
			   return response;
		}
		}
    /*
     * LogOut Successfully
     * */
    @PostMapping("/destroy")
	public BaseResponse destroySession()
    {
    	BaseResponse response=new BaseResponse(true);
		//req.getSession().invalidate();
    	 HttpSession session = req.getSession();
		String ses=(String) session.getAttribute("SESSION");
		if(session.getAttribute("SESSION")!=null)
		{
			req.getSession().invalidate();
			response.setSuccess(true);
			response.addMessage(BaseResponse.LOGOUT);
			return response;
		}
		else 
		{
    	response.setSuccess(true);
		response.addMessage(BaseResponse.FAILED);
		return response;
	   }
   
    }
	 
}
