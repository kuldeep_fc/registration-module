package com.deep.code.login;
/*
 * Login Repository
 * 
 * Author :- Kuldeep Argal 29/07/2020
 * 
 * */
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;



@Repository
public interface LoginRepository extends JpaRepository<LoginDTO,Long>
{
	@Query(value="loginByEmail :email", nativeQuery = true)
	public LoginDTO loginByEmail(@Param("email") String email);
}
